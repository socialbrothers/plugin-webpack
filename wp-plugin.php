<?php

/**
 * @package socialbrothers/wp-plugin
 * @author  : Social Brothers, Author
 * @wordpress-plugin
 * Plugin Name:       wp-plugin
 * Description:       plugin
 * Plugin Version:    1.0.x-dev
 * Requires at least: 5.2
 * Requires PHP:      7.3
 */

/**
 * Define plugin root dir, the location where __DIR__ is called, effects it's outcome, also when used in a function
 * defined elsewhere. That's why it's recommended to set a PHP Constant at the beginning of the main plugin file.
 */
if (! defined('WP_PLUGIN_ROOT_PATH')) {
    define('WP_PLUGIN_ROOT_PATH', __DIR__);
}


function register_scripts()
{
    wp_enqueue_script('main-js', plugins_url('/public/main.bundle.js', __FILE__), [], time(), true);
    wp_localize_script('main-js', 'plugin_ajax_url', ['ajaxurl' => admin_url('admin-ajax.php')]);
}
add_action('wp_enqueue_scripts', 'register_scripts');


function insert_template()
{
    ob_start();
    include(WP_PLUGIN_ROOT_PATH . '/templates/template.php');

    return ob_get_clean();
}


function shortcode_register()
{
    add_shortcode('load_template', 'insert_template');
}

add_action('init', 'shortcode_register');
