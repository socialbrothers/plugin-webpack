const path = require('path');
module.exports = {
    watch: true,
    mode: "development",
    entry: {
        main: path.resolve(__dirname, "resources/index.js"),
    },
    output: {
        filename: "main.bundle.js",
        path: path.resolve(__dirname, "public")
    },
    module: {
        rules: [
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            },
            {
                test: /\.?s[ac]ss$/i,
                use: ["style-loader", "css-loader", "sass-loader"],
            }
        ],
    },
}
